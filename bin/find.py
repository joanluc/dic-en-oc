#!/usr/bin/env python

# Check out genfind.py from David M. Beazley.

# genfind.py
#
# A function that generates files that match a given filename pattern

import os
import fnmatch

def gen_find(filepat,top):
    for path, dirlist, filelist in os.walk(top):
        for name in fnmatch.filter(filelist,filepat):
            yield os.path.join(path,name)

# Example use

if __name__ == '__main__':
    poFiles = gen_find("*.po","~/")
    for name in poFiles:
        print(name)

